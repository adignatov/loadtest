package org.fxclub;

import org.fxclub.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ratpack.handling.Handler;
import ratpack.spring.config.EnableRatpack;


@SpringBootApplication
@EnableRatpack
public class LoadtestApplication {
    @Autowired
    TestService testService;

    @Bean
    public Handler handler() {
        return context -> context.render(testService.test(10000));
    }

    public static void main(String[] args) {
        SpringApplication.run(LoadtestApplication.class, args);
    }
}
