package org.fxclub.service;

import org.springframework.stereotype.Service;

@Service
public class TestService {

    public String test(int delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "ok";
    }

}
