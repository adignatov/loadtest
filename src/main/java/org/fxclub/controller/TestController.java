package org.fxclub.controller;

import org.fxclub.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import rx.Observable;
import rx.schedulers.Schedulers;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping(value = "/loadtest")
public class TestController {
    @Autowired
    TestService testService;

    private final ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(100);

    @RequestMapping(method = RequestMethod.GET, value = "/block")
    public String block() {
        return testService.test(10000);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/non_block")
    public DeferredResult<String> nonBlockAsync() {
        DeferredResult<String> deferredResult = new DeferredResult<>();
        Observable.fromCallable(() -> testService.test(10000))
                .subscribeOn(Schedulers.from(threadPoolExecutor))
                .subscribe(s -> {
                    if (!deferredResult.isSetOrExpired())
                        deferredResult.setResult(s);
                });
        return deferredResult;
    }
}
